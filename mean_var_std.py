import numpy as np

def calculate(list):
    if len(list) < 9:
        raise ValueError("List must contain nine numbers.")

    np_array = np.array(list).reshape(3, 3)

    calculations = {
        {"std": "standard deviation", "var": "variance"}.get(stat, stat): [
            getattr(np, stat)(np_array, axis=axis).tolist()
            for axis in (0, 1, None)
        ]
        for stat in ["mean", "var", "std", "max", "min", "sum"]
    }

    return calculations
